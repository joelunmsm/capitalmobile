angular.module('app.controllers', ['ionic'])
 
.controller('loginCtrl', ['$scope','$stateParams','$http','$localStorage','$state','$location','$filter','$timeout','$ionicHistory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope,$stateParams,$http,$localStorage,$state,$location,$filter,$timeout,$ionicHistory) {



    function urlBase64Decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
        switch (output.length % 4) {
            case 0:
                break;
            case 2:
                output += '==';
                break;
            case 3:
                output += '=';
                break;
            default:
                throw 'Illegal base64url string!';
        }
        return window.atob(output);
    }


    function getUserFromToken() {
        var token = $localStorage.token;
        var user = {};
        if (typeof token !== 'undefined') {
            var encoded = token.split('.')[1];
            user = JSON.parse(urlBase64Decode(encoded));
        }
        return user;
    }

    $scope.logeandose=0

    $scope.ingresar = function (data) {


            $timeout(function() {
                $ionicHistory.clearCache();
                $ionicHistory.clearHistory();
            }, 100);
         
          $scope.logeandose=1

          $localStorage.broker=false

           $http({

            url: host+'/api-token-auth/',
            data: data,
            method: 'POST'
            }).
            success(function(data) {

            $localStorage.token = data.token;

            var currentUser = getUserFromToken();

            console.log('user logeado',currentUser)

            if($localStorage.token){

                $scope.logeandose=0

                $scope.errors=''

                    $http.get(host+"/agente/").success(function(response) {

                      console.log('logeandose',response)

                        if(response[0].tipo_agente__nombre=='Broker'){

                          $localStorage.broker=true
                        }

                        $state.go('inicio');
                      
                      
                    });

            }


           })
            .error(function(data){

              $scope.errors = data.errors.__all__

              $scope.logeandose=0


            })

    }

}])


// Adicionales Cointrollers


   
.controller('brokerCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$state','$ionicPopup','$ionicHistory','$window','$rootScope','$ionicLoading','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller

function ($scope,$stateParams,$http,$localStorage,$location,$state,$ionicPopup,$ionicHistory,$window,$rootScope,$ionicLoading,$filter) {


   

  $scope.iraxiencias = function(){

    console.log('xienciass fuck.......')

    location.href ="http://xiencias.com/cfp";

  }


  $scope.label= true

  console.log('miperfilctrl')

   $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando tu perfil...',
       duration:1000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



    $http.get(host+"/agente/").success(function(response) {


      console.log('agente....',response)


      $scope.agente = response[0];

      $scope.agente.fecha_ingreso = new Date($scope.agente.fecha_ingreso)

      $scope.agente.fecha_nacimiento = new Date($scope.agente.fecha_nacimiento)

      console.log('data',$scope.agente)


    });


  function getMobileOperatingSystem() {
  

    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {


        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {


        $http.get(host+"/asignamovil/0").success(function(response) {});

        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {


        $http.get(host+"/asignamovil/1").success(function(response) {});

        return "iOS";
    }

    return "unknown";

    }




    $scope.errorversionios = function(data) {
      $scope.data = {}


    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">Tienes una version antigua de la aplicacion de Capital, por favor actualizar la app en <a href="http://xiencias.com/cfp">http://xiencias.com/cfp</a> <li>',
         
    

         scope: $scope,

         title:'Advertencia:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-light'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.errorversionandroid = function(data) {

      $scope.data = {}

      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">Tienes una version antigua de la aplicacion de Capital, por favor actualizar la app en el  <a href="https://play.google.com/store/apps/details?id=com.capital.peru">Play Store</a> <li>',
         
    

         scope: $scope,

         title:'Advertencia:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-light'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };





    $http.get(host+"/verificaversion/").success(function(response) {

      console.log('Verificando version...',response)

      if(getMobileOperatingSystem()=='iOS'){

                if(response=='Sin actualizar'){

                $scope.errorversionios()

                }

      }

      if(getMobileOperatingSystem()=='android'){


          if(response=='Sin actualizar'){

          $scope.errorversionandroid()

          }


      }







    });


    // $http.get(host+"/version/55").success(function(response) {


    // });


    $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});


    $scope.setFile = function(element) {

    $scope.photo_1 = false

    $scope.statusimagen = 'Cargando imagen...'

    $scope.currentFile = element.files[0];


    var reader = new FileReader();

    reader.onload = function(event) {

    $scope.image = event.target.result

    $scope.$apply()

    }
    
    reader.readAsDataURL(element.files[0]);

    var file = $scope.myFile;

    var fd = new FormData();

    fd.append('file', $scope.currentFile);

    $http.post(host+'/uploadphoto/', fd, {
    transformRequest: angular.identity,
    headers: {'Content-Type': undefined}
    })
    

    .success(function(data){


      $http.get(host+"/agente/").success(function(response) {


        $scope.agente = response[0];



      });


    })



    }



    $scope.host= host

        $scope.okficha = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo datos correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         
      });    
   };

  
    



    $scope.salir = function () {


    $rootScope = $rootScope.$new(true);
    $scope = $scope.$new(true);

       // $window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    delete $localStorage;

      //ionic.Platform.exitApp(); // para la aplicacion
    
     


    }





 $scope.muestra = true

 $scope.actualiza=function(data){


   console.log('Actualizando agente...')

   $scope.muestra =false

   $scope.guarda()

   $http({

            url: host+"/agente/",
            data: data,
            method: 'PUT'
            }).
            success(function(data) {

              


              $scope.muestra =true

           })
 }



}])

   
.controller('capitalCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$state','$ionicPopup','$ionicHistory','$window','$rootScope','$ionicLoading','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller

function ($scope,$stateParams,$http,$localStorage,$location,$state,$ionicPopup,$ionicHistory,$window,$rootScope,$ionicLoading,$filter) {




  console.log('localStorage.....',$localStorage.broker)

  $scope.broker = $localStorage.broker


  $scope.iraxiencias = function(){

    console.log('xienciass fuck.......')

    location.href ="http://xiencias.com/cfp";

  }


  $scope.label= true

  console.log('miperfilctrl')

        $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando tu perfil...',
       duration:1000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



    $http.get(host+"/agente/").success(function(response) {

      $scope.agente = response[0];

      $scope.agente.fecha_ingreso = new Date($scope.agente.fecha_ingreso)

      $scope.agente.fecha_nacimiento = new Date($scope.agente.fecha_nacimiento)

      console.log('data',$scope.agente)

    });


       function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {


        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {


        $http.get(host+"/asignamovil/0").success(function(response) {});

        return "Android";
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {


        $http.get(host+"/asignamovil/1").success(function(response) {});

        return "iOS";
    }

    return "unknown";

    }


     console.log('hshshhs',getMobileOperatingSystem())



    $scope.errorversionios = function(data) {
      $scope.data = {}


    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">Tienes una version antigua de la aplicacion de Capital, por favor actualizar la app en <a href="http://xiencias.com/cfp">http://xiencias.com/cfp</a> <li>',
         
    

         scope: $scope,

         title:'Advertencia:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-light'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.errorversionandroid = function(data) {
      $scope.data = {}


    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">Tienes una version antigua de la aplicacion de Capital, por favor actualizar la app en el  <a href="https://play.google.com/store/apps/details?id=com.capital.peru">Play Store</a> <li>',
         
    

         scope: $scope,

         title:'Advertencia:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-light'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };





    $http.get(host+"/verificaversion/").success(function(response) {


      console.log('Verificando version...',response)

      if(getMobileOperatingSystem()=='iOS'){

                if(response=='Sin actualizar'){

                $scope.errorversionios()

                }


      }

      if(getMobileOperatingSystem()=='android'){

                if(response=='Sin actualizar'){

                $scope.errorversionandroid()

                }


      }







    });



    document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {


    var data={
      modelo:device.model,
      version:device.version,
      uuid:device.uuid,
      platform:device.platform
     }


      $http({

                  url: host+"/guardadevice",
                  data: data,
                  method: 'POST'
                  }).
                  success(function(data) {


                   



            })
}



    $http.get(host+"/version/0526").success(function(response) {


    });


    $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});


    $scope.setFile = function(element) {

    $scope.photo_1 = false

    $scope.statusimagen = 'Cargando imagen...'

    $scope.currentFile = element.files[0];


    var reader = new FileReader();

    reader.onload = function(event) {

    $scope.image = event.target.result

    $scope.$apply()

    }
    
    reader.readAsDataURL(element.files[0]);

    var file = $scope.myFile;

    var fd = new FormData();

    fd.append('file', $scope.currentFile);

    $http.post(host+'/uploadphoto/', fd, {
    transformRequest: angular.identity,
    headers: {'Content-Type': undefined}
    })
    

    .success(function(data){


      $http.get(host+"/agente/").success(function(response) {


        $scope.agente = response[0];



      });


    })



    }



    $scope.host= host

        $scope.okficha = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo datos correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         
      });    
   };

  
    



    $scope.salir = function () {


    $rootScope = $rootScope.$new(true);
    $scope = $scope.$new(true);

       // $window.localStorage.clear();
    $ionicHistory.clearCache();
    $ionicHistory.clearHistory();

    delete $localStorage;

      //ionic.Platform.exitApp(); // para la aplicacion
    
     


    }





 $scope.muestra = true

 $scope.actualiza=function(data){


   console.log('Actualizando agente...')

   $scope.muestra =false

   $scope.guarda()

   $http({

            url: host+"/agente/",
            data: data,
            method: 'PUT'
            }).
            success(function(data) {

              


              $scope.muestra =true

           })
 }



}])
   


.controller('clienteantiguoCtrl', ['$scope', '$stateParams','$http','$location','$rootScope','$ionicLoading','$filter',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$location,$rootScope,$ionicLoading,$filter) {


     $scope.muestra =true

      $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando cliente...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



  $scope.show = function() {
 
   $ionicLoading.show({ template: 'Se guardo con exito el cliente', noBackdrop: true, duration: 2000 })
  
  };
  
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  

  $scope.error = function(data) {
 
   $ionicLoading.show({ template: 'Completar ' + data, noBackdrop: true, duration: 2000 })
  
  };


  $scope.limpiar=function(data){

    console.log(data.direccion)

    data.direccion=null
    data.telefono=null
    data.last_name=null
    data.first_name=null
    data.fecha_inicio=null

   }

     
    $scope.agregacliente=function(data){


        console.log('guarda cliente',data)

      $scope.guarda()

      e=0

      // if (data.fecha_inicio==null){

      //   $scope.error('fecha de cita')

      //   e=1

      // }

      if (data.first_name==null || data.first_name==''){

        $scope.error('nombre')

        e=1
      }


      if (e==0){


                  $http({
          url: host+"/creacliente/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

          
            $scope.hide()
            

            $scope.muestra =true

            $location.url('/agregaprospectoantiguo/'+data)


          })



      }


    }




}])


.controller('nuevoclienteCtrl', ['$scope', '$stateParams','$http','$location','$rootScope','$ionicLoading','$filter',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$location,$rootScope,$ionicLoading,$filter) {


$scope.nuevocliente=false

$scope.allclientes=undefined


$scope.tooltip = false

$scope.titulo=''

$scope.mousenp=function(data){

  console.log('daddadada')

  $scope.tooltip = true

  $scope.titulo=data



}

$scope.nuevoclientemuestra=function(){

  $scope.nuevocliente=true
}


$scope.mouseleave=function(){

  console.log('daddadada')

  $scope.tooltip = false

}





  $scope.listapropuestasget =function(data){


        $location.url('/listapropuestas/'+data)
      }





$http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;

        

        });

$scope.traetodoslosclientes = function(data){


      $scope.allclientes=data

      console.log('traetodosclientes..',data)


      $scope.show()

        $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;

          $scope.hide()

        });
    

    }


 $http.get(host+"/agente/").success(function(response) {$scope.agente=response[0];


console.log('agente',$scope.agente)


 });


     $scope.muestra =true

      $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando cliente...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



      $scope.show = function() {
 
   $ionicLoading.show({ template: 'Se guardo con exito el cliente', noBackdrop: true, duration: 2000 })
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  

      $scope.error = function(data) {
 
   $ionicLoading.show({ template: 'Completar ' + data, noBackdrop: true, duration: 2000 })
  };









   $scope.limpiar=function(data){


    console.log(data.direccion)

    data.direccion=null
    data.telefono=null
    data.last_name=null
    data.first_name=null
    data.fecha_inicio=null



   }

     
    $scope.agregacliente=function(data){

      $scope.guarda()

      e=0
      console.log('agregacliente',data)

      if (data==undefined){

        $scope.error('nombre')

        e=1

      }

      if (data.first_name==null || data.first_name==''){

        $scope.error('nombre')

        e=1
      }

       

      

      if (data.telefono==null || data.telefono==''){

        $scope.error('telefono')

        e=1
      }

       if (data.last_name==null || data.last_name==''){

        $scope.error('apellido')

        e=1
      }



      if (e==0){


                  $http({
          url: host+"/creacliente/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

          
            $scope.hide()
            

            $scope.muestra =true

            $location.url('/agregaprospecto/'+data)


          })



      }


    }




}])
   

.controller('registroDeCitasCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicModal','$ionicPopup','$filter','$state','$ionicHistory','$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicModal,$ionicPopup,$filter,$state,$ionicHistory,$ionicLoading) {

 $http.get(host+"/agente/").success(function(response) {

    $scope.agente=response[0];

  });





   

  //$scope.modal.show();



}])


.controller('detallepropuestaCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicModal','$ionicPopup','$filter','$state','$ionicHistory','$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicModal,$ionicPopup,$filter,$state,$ionicHistory,$ionicLoading) {

  $scope.propuesta=$stateParams.propuesta


  $scope.showPopup = function(data) {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '<span style="color:#000;">'+data+'</span>',
         
         scope: $scope,
      
         buttons: [
            { text: 'Cerrar',
              type: 'button-light',
            }
         ]
      });

      myPopup.then(function(res) {
         console.log('Tapped!', res);
      });    
   };






  $scope.eliminapropuesta = function(data){

  console.log('queeee',data)

  $http.get(host+"/eliminapropuesta/"+data).success(function(response) {

  $ionicHistory.goBack();
  
  });


  }

  $scope.show = function() {
    $ionicLoading.show({
      template: 'Guardanado propuesta',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

   $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }; 


  console.log('detallepropuestaCtrl')

  $http.get(host+"/detallepropuesta/"+$scope.propuesta).success(function(response) {




  $scope.detallepropuesta = response[0];

  console.log('detallepropuesta',$scope.detallepropuesta)

  // $scope.detallepropuesta.fecha = new Date($scope.detallepropuesta.fecha)

  // $scope.detallepropuesta.fecha_poliza = new Date($scope.detallepropuesta.fecha_poliza)

  // $scope.detallepropuesta.fecha_solicitud = new Date($scope.detallepropuesta.fecha_solicitud)

  });

  $scope.actualizarpropuesta =function(data){


    console.log(data)

    $scope.show()

   $http({
          url: host+"/updatepropuesta/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {


            $scope.hide()

           

       })








  }



}])


.controller('prospectoDeClienteCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicModal','$ionicPopup','$filter','$state','$ionicHistory','$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicModal,$ionicPopup,$filter,$state,$ionicHistory,$ionicLoading) {



  $scope.tooltip = false

$scope.titulo=''

$scope.mousenp=function(data){

  console.log('daddadada')

  $scope.tooltip = true

  $scope.titulo=data

}


$scope.mouseleave=function(){

  console.log('daddadada')

  $scope.tooltip = false

}


  $scope.iraclienteantiguo = function(data){

    $location.url('clienteantiguo')

  }



  $scope.show = function() {
    $ionicLoading.show({
      template: 'Cargando informacion...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

    $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  

   $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando propuesta...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.error = function(data) {
    $ionicLoading.show({
      template: 'Ingrese '+data,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };




  $scope.show()
  

  $http.get(host+"/agente/").success(function(response) {

    $scope.agente=response[0];

  });


  $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});

  //$scope.propuesta=$stateParams.propuesta

  $scope.iracliente =function(data){

  $location.url('fichacliente/'+data)

  }

  $scope.eliminapropuesta = function(data){

  $http.get(host+"/eliminapropuesta/"+data).success(function(response) {

  $ionicHistory.goBack();
  
  });


  }




     $scope.traepos = function(data){

       
    $location.url('/citapos/'+data)

    }


      $scope.cliid =$stateParams.cliente

      console.log('$scope.cliid',$scope.cliid)

      if($stateParams.cliente){

                $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) {

     

                $scope.clientepropuesta = response[0];

                $scope.traepropuestas($stateParams.cliente)

                });

      }




 
      $scope.listapropuestasget =function(data){


        $location.url('/listapropuestas/'+data)
      }



      $scope.traepropuestas = function(data){

      $http.get(host+"/listapropuestas/"+data).success(function(response) {


                  console.log('¡listapropuestas',response)

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})


                  $scope.listapropuestascerradas=$filter('filter')(response,{"cierre" : 1})



                  });

    }


$scope.ocultalupa =true

    $ionicModal.fromTemplateUrl('templates/nuevocliente.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.nuevoclientemodal = modal;
   });

   $scope.nuevoclienteopenModal = function(data) {

   

      $scope.nuevoclientemodal.show();
   };
    
   $scope.nuevoclientecloseModal = function() {
      $scope.nuevoclientemodal.hide();
   };

/////////
    
  $ionicModal.fromTemplateUrl('templates/my-modal.html', {
      scope: $scope,
      cache:false,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modal = modal;
   });

   $scope.openModal = function(data) {


    $scope.propuesta = {}

    $scope.propuesta.cliente = data

      $scope.modal.show();

   };



    
   $scope.closeModal = function() {

      $scope.modal.hide();

      console.log('cerrando modal...')
      
      delete $scope.ramo

      delete $scope.cia

      delete $scope.producto

      delete $scope.interes



   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });



  
       /// Popup



    $scope.okPropuestaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo propuesta correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         $scope.closeModal()
      });    
   };

    $scope.okErrorPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Agente = Cliente ?',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         ('Tapped!', res);
      });    
   };

    $scope.okErrorEmail = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Este correo ya existe, porfavor ingrese otro correo',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
     
      });    
   };





    $http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});

     $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;

    $scope.hide()

     });
    
     $scope.traetodoslosclientes = function(data){


      $scope.show()

        $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;

          $scope.hide()

        });
    

    }

     $scope.datosadicionales = function(data){

       

        $location.url('/fichacliente/'+data)
    }

    $scope.searchb=false


    $scope.setinteres=function(data){

    $scope.interes = data

    $scope.propuesta.interes = data

   }


    $scope.obtenercia = function(data){


      console.log('obtener cia..',data)

      $scope.ramo = data

      $scope.propuesta.ramo = data

      $scope.producto=null

       $scope.cia =null

       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

            $scope.cias = response;});

    }

    $scope.obtenerpropuesta = function(data){

      $scope.propuesta.producto = data

      $scope.producto=data
    }


    $scope.obtenerproducto = function(data){


        console.log('producto..',data)

        $scope.propuesta.cia =data


        $scope.cia =data

    
          $http.get(host+"/listaproducto/"+$scope.ramo.id+'/'+$scope.cia.compania).success(function(response) {

             $scope.productos = response;


            console.log('obtenerproducto',response)


         });

    }

    $scope.traepropuestas = function(data){


      $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')(response,{"cierre" : 0})


                     $scope.listapropuestascerradas=$filter('filter')(response,{"cierre" : 1})



                  });

    }



     $scope.muestra =true


    $scope.agregacliente=function(data){


      console.log('agregacliente',data)


    $scope.muestra =false


            $http({
            url: host+"/creacliente/",
            data: data,
            method: 'POST',

            }).
            success(function(data) {




                    $http.get(host+"/cliente/"+data).success(function(response) {

      

                    $scope.clientesave = response[0];

                    });

                    $scope.nuevoclientemodal.hide()
                     $scope.muestra =true








            })

    }

    ///Busca cliente

 
    $scope.traecliente=function(data){


      console.log('trayendo clientes...')


      $http.get(host+"/cliente/"+data).success(function(response) {

                $scope.clientesave = response[0];

                $scope.todosclientes=''

                $scope.traepropuestas(data)

               

                $scope.hide()
                

            });



    }

    $scope.mostrar=true

    $scope.agregapropuesta=function(data){





            console.log('Agregando propuesta..',data)



            

            data.seguimientonp = 1 

            if (data.cia.compania){

              $scope.guarda()

              data.cia = data.cia.compania

            }
            else{

              $scope.error('compañia')

            }



            data.producto = data.producto.producto

            $scope.mostrar=false


            console.log('enviando....',data)

            $http({

            url: host+"/creapropuesta/",
            data: data,
            method: 'POST',

            }).
            success(function(data) {

                //$scope.okPropuestaPopup()

                $scope.hide()

                $scope.closeModal()

                $http.get(host+"/listapropuestas/"+data).success(function(response) {

                $scope.listapropuestas = response;

                $scope.listapropuestas=$filter('filter')(response,{"cierre" : 0})


                   $scope.listapropuestascerradas=$filter('filter')(response,{"cierre" : 1})



                });
                
            }).

                  error(function(data) {

                     $scope.hide()

             


    
               });

          

    }



    ////Modales


     $scope.showPopup = function() {


      console.log('ingresando.....')


      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in ramos" ng-click="obtenercia(item);cierra()">{{item.nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup1 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in cias" ng-click="obtenerproducto(item);cierra()">{{item.compania__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup2 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in productos" ng-click="obtenerpropuesta(item);cierra()">{{item.producto__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };


      $scope.intereses = [
        {name: 'Muy Interesado'},
        {name: 'Interesado'},
        {name: 'Interesado en el futuro'},
        {name: 'No Interesado'}
    
      ];


      $scope.showPopup3 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in intereses" ng-click="setinteres(item);cierra()">{{item.name}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   


}])



.controller('agregaprospectoantiguoCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$ionicModal','$ionicPopup','$ionicLoading','$filter',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$ionicModal,$ionicPopup,$ionicLoading,$filter) {


  console.log('cliente..',$stateParams.cliente)





  $http.get(host+"/modalidad/").success(function(response) {$scope.modalidad = response;});

    $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando prospecto...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

   $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };


   $scope.alertausuarioviejo = function() {
    $ionicLoading.show({
      template: 'Se esta creando un usuario antiguo ',
      duration:9000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.muestrausuarioviejo = function() {
    $ionicLoading.show({
      template: 'Esta opcion es para los cliente antiguos, y se creara un cierre inforce automaticamente',
      duration:3000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.cambiausuario=function(data){


    console.log(data)

    if(data==true){

      $scope.muestrausuarioviejo()

    }



  }




/// Trae cliente
if($stateParams.cliente){



           console.log('entre aqui.....')

          $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) {


     


          $scope.cliente = response[0];

          $scope.traepropuestas($stateParams.cliente)

          });

}




 $scope.showPopup = function() {


      console.log('ingresando.....')


      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in ramos" ng-click="obtenercia(item);cierra()">{{item.nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup1 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in cias" ng-click="obtenerproducto(item);cierra()">{{item.compania__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup2 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in productos" ng-click="obtenerpropuesta(item);cierra()">{{item.producto__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          console.log('gsgsgsg')

          myPopup1.close()
         }   
   };


    // $scope.alertausuarioviejo = function() {
    //   $scope.data = {}
    
    //   // Custom popup
    //   var myPopupuser = $ionicPopup.show({
    //      title: 'Esta apunto de crear un usuario antiguo y crear sus cierre con la fecha, desea continuar...? ',
    //      scope: $scope,
         
    //      buttons: [
    //         { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

    //         { text: 'Guardar',type: 'button-light',onTap: function(e) {$scope.alertausuarioviejo()}}
                     
    //      ]

    //   });


   //     $scope.cierra=function(){
   //        myPopupuser.close()
   //       }   
   // };


      $scope.intereses = [
        {name: 'Muy Interesado'},
        {name: 'Interesado'},
        {name: 'Interesado en el futuro'},
        {name: 'No Interesado'}
    
      ];


      $scope.showPopup3 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in intereses" ng-click="setinteres(item);cierra()">{{item.name}}</li>',
         
    

         scope: $scope,

         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.error = function(data) {
      $scope.data = {}

      delete $scope.ramo

      delete $scope.cia

      delete $scope.producto

      delete $scope.interes
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">'+data+' <li>',
         
    

         scope: $scope,

         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };




 $ionicModal.fromTemplateUrl('templates/propuestaantigua.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modal = modal;
   });




    $scope.propuesta = {}

   $scope.openModal = function(data) {



    $scope.propuesta.cliente = data


   

      $scope.modal.show();

   };
    
   $scope.closeModal = function() {
      $scope.modal.hide();

      delete $scope.ramo

      delete $scope.cia

      delete $scope.producto

      delete $scope.interes

      delete $scope.observacion
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });



  
       /// Popup



    $scope.okPropuestaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo propuesta correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         $scope.closeModal()
      });    
   };

    $scope.okErrorPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Agente = Cliente ?',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

      });    
   };






    $http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});

     $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response; });
    
     $scope.traetodoslosclientes = function(data){

        $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;});
    

    }

     $scope.datosadicionales = function(data){


        $location.url('/fichacliente/'+data)
    }

    $scope.searchb=false


    $scope.setinteres=function(data){

    $scope.interes = data

    $scope.propuesta.interes = data

   }


    $scope.obtenercia = function(data){


      console.log('obtener cia..',data)

      $scope.ramo = data

      $scope.propuesta.ramo = data

      $scope.producto=null

       $scope.cia =null

       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

            $scope.cias = response;});

    }

    $scope.obtenerpropuesta = function(data){

      $scope.propuesta.producto = data

      $scope.producto=data
    }


    $scope.obtenerproducto = function(data){



        $scope.propuesta.cia =data


        $scope.cia =data

    
          $http.get(host+"/listaproducto/"+$scope.ramo.id+'/'+$scope.cia.compania).success(function(response) {

             $scope.productos = response;


            console.log('obtenerproducto',response)


         });

    }

    $scope.traepropuestas = function(data){


      $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  //$scope.listapropuestas=$filter('filter')(response,{"cierre" : 0})


                  });

    }



     $scope.muestra =true


    $scope.agregacliente=function(data){

      console.log('agregacliente',data)
    $scope.muestra =false


            $http({
            url: host+"/creacliente/",
            data: data,
            method: 'POST',

            }).
            success(function(data) {




                    $http.get(host+"/cliente/"+data).success(function(response) {

      

                    $scope.clientesave = response[0];

                    });

                    $scope.nuevoclientemodal.hide()
                     $scope.muestra =true








            })

    }

    ///Busca cliente

 
    $scope.traecliente=function(data){


      $http.get(host+"/cliente/"+data).success(function(response) {

                $scope.clientesave = response[0];

                $scope.todosclientes=''

                $scope.traepropuestas(data)

               


                

            });



    }

    $scope.mostrar=true




    $scope.agregapropuesta=function(data){


                  console.log('agregando propuesta',data)

                  data.isChecked=true


                  data.clienteantiguo = 1



                  

                  if (!data.cia){$scope.error('Error, porfavor completar la rama, compania o producto')}

                  data.cia = data.cia.compania

                  if (!data.producto){$scope.error('Error, porfavor completar la rama, compania o producto')}

                  data.producto = data.producto.producto

                  $scope.mostrar=false

                   if(data.isChecked==true){


                          $scope.alertausuarioviejo()



                  }
                  else{

                      $scope.guarda()

                  }


                  

                  $http({
                  url: host+"/creapropuesta/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {

                  //$scope.okPropuestaPopup()

                  $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  //$scope.listapropuestas=$filter('filter')(response,{"cierre" : 0})


                  $scope.closeModal()

                  $scope.hide()

                  delete $scope.propuesta.prima_target

                  delete $scope.propuesta.prima_anual

                  delete $scope.propuesta.fecha_poliza

                  delete $scope.propuesta.modalidad

                  delete $scope.propuesta.observacion



                  })

                  })
                  .
                  error(function(data) {

                  $scope.hide()

                  $scope.error('Error, porfavor completar la rama, compania o producto')


                  });

          

    }




}])

   

.controller('agregaprospectoCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$ionicModal','$ionicPopup','$ionicLoading','$filter',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$ionicModal,$ionicPopup,$ionicLoading,$filter) {


  console.log('cliente..',$stateParams.cliente)


      $http.get(host+"/agente/").success(function(response) {


      $scope.agente = response[0];

      $scope.agente.fecha_ingreso = new Date($scope.agente.fecha_ingreso)

      $scope.agente.fecha_nacimiento = new Date($scope.agente.fecha_nacimiento)

      console.log('data',$scope.agente)



    });



  $http.get(host+"/modalidad/").success(function(response) {$scope.modalidad = response;});

    $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando prospecto...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

   $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };


   $scope.alertausuarioviejo = function() {
    $ionicLoading.show({
      template: 'Se esta creando un usuario antiguo ',
      duration:9000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.muestrausuarioviejo = function() {
    $ionicLoading.show({
      template: 'Esta opcion es para los cliente antiguos, y se creara un cierre inforce automaticamente',
      duration:3000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  $scope.cambiausuario=function(data){


    console.log(data)

    if(data==true){

      $scope.muestrausuarioviejo()

    }



  }




/// Trae cliente
if($stateParams.cliente){



           console.log('entre aqui.....')

          $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) {


     


          $scope.cliente = response[0];

          $scope.traepropuestas($stateParams.cliente)

          });

}




 $scope.showPopup = function() {


      console.log('ingresando.....')


      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in ramos" ng-click="obtenercia(item);cierra()">{{item.nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup1 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in cias" ng-click="obtenerproducto(item);cierra()">{{item.compania__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup2 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in productos" ng-click="obtenerpropuesta(item);cierra()">{{item.producto__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };


    // $scope.alertausuarioviejo = function() {
    //   $scope.data = {}
    
    //   // Custom popup
    //   var myPopupuser = $ionicPopup.show({
    //      title: 'Esta apunto de crear un usuario antiguo y crear sus cierre con la fecha, desea continuar...? ',
    //      scope: $scope,
         
    //      buttons: [
    //         { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

    //         { text: 'Guardar',type: 'button-light',onTap: function(e) {$scope.alertausuarioviejo()}}
                     
    //      ]

    //   });


   //     $scope.cierra=function(){
   //        myPopupuser.close()
   //       }   
   // };


      $scope.intereses = [
        {name: 'Muy Interesado'},
        {name: 'Interesado'},
        {name: 'Interesado en el futuro'},
        {name: 'No Interesado'}
    
      ];


      $scope.showPopup3 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in intereses" ng-click="setinteres(item);cierra()">{{item.name}}</li>',
         
    

         scope: $scope,

         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.error = function(data) {
      $scope.data = {}

      delete $scope.ramo

      delete $scope.cia

      delete $scope.producto

      delete $scope.interes
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border-radius: 2px;color: #000;margin-bottom: 2px;">'+data+' <li>',
         
    

         scope: $scope,

         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };




 $ionicModal.fromTemplateUrl('templates/my-modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modal = modal;
   });




    $scope.propuesta = {}

   $scope.openModal = function(data) {



    $scope.propuesta.cliente = data


   

      $scope.modal.show();

   };
    
   $scope.closeModal = function() {
      $scope.modal.hide();

      delete $scope.ramo

      delete $scope.cia

      delete $scope.producto

      delete $scope.interes
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });



  
       /// Popup



    $scope.okPropuestaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo propuesta correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         $scope.closeModal()
      });    
   };

    $scope.okErrorPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Agente = Cliente ?',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

      });    
   };






    $http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});

     $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response; });
    
     $scope.traetodoslosclientes = function(data){

        $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;});
    

    }

     $scope.datosadicionales = function(data){


        $location.url('/fichacliente/'+data)
    }

    $scope.searchb=false


    $scope.setinteres=function(data){

    $scope.interes = data

    $scope.propuesta.interes = data

   }


    $scope.obtenercia = function(data){


      console.log('obtener cia..',data)

      $scope.ramo = data

      $scope.propuesta.ramo = data

      $scope.producto=null

       $scope.cia =null

       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

            $scope.cias = response;});

    }

    $scope.obtenerpropuesta = function(data){

      $scope.propuesta.producto = data

      $scope.producto=data
    }


    $scope.obtenerproducto = function(data){



        $scope.propuesta.cia =data


        $scope.cia =data

    
          $http.get(host+"/listaproducto/"+$scope.ramo.id+'/'+$scope.cia.compania).success(function(response) {

             $scope.productos = response;


            console.log('obtenerproducto',response)


         });

    }

    $scope.traepropuestas = function(data){


      $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')(response,{"cierre" : 0})


                  });

    }



     $scope.muestra =true


    $scope.agregacliente=function(data){


      console.log('agregacliente',data)
    $scope.muestra =false


            $http({
            url: host+"/creacliente/",
            data: data,
            method: 'POST',

            }).
            success(function(data) {

                    $http.get(host+"/cliente/"+data).success(function(response) {

                      $scope.clientesave = response[0];

                    });

                        $scope.nuevoclientemodal.hide()
                        $scope.muestra =true








            })

    }

    ///Busca cliente

 
    $scope.traecliente=function(data){


      $http.get(host+"/cliente/"+data).success(function(response) {

                $scope.clientesave = response[0];

                $scope.todosclientes=''

                $scope.traepropuestas(data)

               


                

            });



    }

    $scope.mostrar=true




    $scope.agregapropuesta=function(data){



                  data.isChecked = false



                  

                  if (!data.cia){$scope.error('Error, porfavor completar la rama, compania o producto')}

                  data.cia = data.cia.compania

                  if (!data.producto){$scope.error('Error, porfavor completar la rama, compania o producto')}

                  data.producto = data.producto.producto

                  data.fecha_cita = $scope.fecha_cita

                  $scope.mostrar=false

                   if(data.isChecked==true){


                          $scope.alertausuarioviejo()



                  }
                  else{

                      $scope.guarda()

                  }


                  

                  $http({
                  url: host+"/creapropuesta/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {

                  //$scope.okPropuestaPopup()

                  $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})


                  $scope.closeModal()

                  $scope.hide()


                  delete $scope.propuesta.observacion




                  })

                  })
                  .
                  error(function(data) {

                  $scope.hide()

                  $scope.error('Error, porfavor completar la rama, compania o producto')


                  });

          

    }




}])
   

   
.controller('pOSCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicModal','$ionicPopup','$ionicHistory','$filter','$ionicLoading',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicModal,$ionicPopup,$ionicHistory,$filter,$ionicLoading) {

    $scope.tooltip = false

$scope.titulo=''

$scope.mousenp=function(data){

  console.log('daddadada')

  $scope.tooltip = true

  $scope.titulo=data

}


$scope.mouseleave=function(){

  console.log('daddadada')

  $scope.tooltip = false

}


     $scope.preload = function() {
    $ionicLoading.show({
      template: 'Cargando informacion...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



     $scope.show = function() {
    $ionicLoading.show({
      template: 'Completar POS',
      duration:1000
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

  



    $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

  $scope.muestra = true

  $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});

    $ionicModal.fromTemplateUrl('templates/nuevocliente.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.nuevoclientemodal = modal;
   });

   $scope.nuevoclienteopenModal = function(data) {



      $scope.nuevoclientemodal.show();
   };
    
   $scope.nuevoclientecloseModal = function() {
      $scope.nuevoclientemodal.hide();
   };


   $scope.creapos=function(data){


          $scope.preload()

          console.log(data.pos)

          if(data.pos){

            $scope.muestra = false

          data.tipo_cita=3
          data.cliente=$stateParams.cliente

          $http({
                  url: host+"/creapos/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {

                    //$scope.okCitaPopup()

                    $scope.hide()

                    $scope.muestra = true

                    $location.url("/side-menu21/page19")

                    

    
               })


          }
          else{

            $scope.show()


          }





   }







/////////
    
     
  $ionicModal.fromTemplateUrl('templates/my-modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modal = modal;
   });

   $scope.openModal = function(data) {


    $scope.propuesta = {}

    $scope.propuesta.cliente = data

      $scope.modal.show();
   };
    
   $scope.closeModal = function() {
      $scope.modal.hide();
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });



  
       /// Popup


           $scope.okCitaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo la cita correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

        //$scope.myGoBack()

        //location.reload();
         
      });    
   };






    $scope.okPropuestaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo propuesta correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         $scope.closeModal()
      });    
   };

       $scope.okErrorPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Agente = Cliente ?',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

      });    
   };






    $http.get(host+"/listapropuestas/"+$stateParams.cliente).success(function(response) {


      $scope.productosdecliente = response;

      ///Filtrar productos activos

      $scope.productosdecliente=$filter('filter')($scope.productosdecliente,{"cierre" : 1})

      


    });


    $http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});

     $http.get(host+"/clientes/").success(function(response) {

      $scope.todosclientes = response;


      $scope.todosclientes=$filter('filter')($scope.todosclientes,{"cierre" : true})



    });
    
     $scope.traetodoslosclientes = function(data){

        $http.get(host+"/clientes/").success(function(response) {



          $scope.todosclientes = response;


          $scope.todosclientes=$filter('filter')($scope.todosclientes,{"cierre" : true})






        });
    

    }

    $scope.searchb=false


    $scope.obtenercia = function(data){



       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

            $scope.cias = response;});

    }

    

    $scope.traepropuestas = function(data){


      $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;


                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})




                  });

    }




    $scope.obtenerproducto = function(data){

    
         $http.get(host+"/listaproducto/"+$scope.propuesta.ramo.id+'/'+$scope.propuesta.cia).success(function(response) {

            $scope.productos = response;

            console.log('response',response)


        });

    }

    $scope.agregacliente=function(data){


            console.log('agregacliente',data)


            $http({
            url: host+"/creacliente/",
            data: data,
            method: 'POST',

            }).
            success(function(data) {





             $http.get(host+"/cliente/"+data).success(function(response) {

                $scope.clientesave = response[0];



            });

              $scope.nuevoclientemodal.hide()

            })

    }

    ///Busca cliente



 
    $scope.traecliente=function(data){


      $http.get(host+"/cliente/"+data).success(function(response) {

                $scope.clientesave = response[0];

                $scope.todosclientes=''

                $scope.traepropuestas(data)

               


                

            });



    }
    

    $scope.traecliente($stateParams.cliente)

     $scope.mostrar=true
     

    $scope.agregapropuesta=function(data){


               $scope.mostrar=false


               console.log('agregando proupesta',data)



                  $http({
                  url: host+"/creapropuesta/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {

                  $scope.okPropuestaPopup()

                  $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})



                  //$scope.okCitaPopup()




                  });



                
            }).
                  error(function(data) {

                     $scope.hide()

              



    
               });

          

    }

   


}])



.controller('perfilbrokerCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$filter) {


    
 $http.get(host+"/agente/").success(function(response) {$scope.agente=response[0];});


 $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});
 


}])
   
.controller('miPerfilCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$filter) {


    
 $http.get(host+"/agente/").success(function(response) {$scope.agente=response[0];});


 $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});
 


}])


.controller('notificacionCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$filter) {

$scope.actualiza = function() {

  $http.get(host+"/notificaciones/").success(function(response) {

    console.log('ENtre.......')

    $scope.notificaciones = response;});

}

    
 $http.get(host+"/agente/").success(function(response) {$scope.agente=response[0];});


 $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});
 
  $http.get(host+"/notificaciones/").success(function(response) {

    console.log('ENtre.......')

    $scope.notificaciones = response;});
 

}])
   
.controller('introCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])


.controller('bibliotecaCtrl', ['$scope', '$stateParams','$http','$ionicLoading','$localStorage',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$ionicLoading,$localStorage) {


   $scope.broker = $localStorage.broker

   $scope.show = function() {
    $ionicLoading.show({
      template: 'Buscando Archivos',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

    $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  }; 

    $scope.buscaarchivos = function(data){


      console.log(data)

      $scope.files={}

      $scope.show()

       $http({
            url: host+"/buscaarchivos",
            data: data,
            method: 'POST',

            }).
            success(function(data) {


              $scope.files = data

              $scope.hide()



            })




    }


    $scope.obtenercia = function(data){





      console.log('obtener cia..',data)

      if (data){

              $scope.ramo = data

  

       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

          console.log('response',response)

            $scope.cias = response;});



      }
      else{

        $scope.cias = {}
      }


    }

    $http.get(host+"/losarchivos/").success(function(response) {$scope.archivos = response;


    });


     $http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});


}])


.controller('reporteCtrl', ['$scope', '$stateParams','$http','$ionicPopup','$ionicModal','$ionicLoading','$location','$filter','$localStorage',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$ionicPopup,$ionicModal,$ionicLoading,$location,$filter,$localStorage) {

/////


$scope.estru = false

$localStorage.estructura = false

 $scope.show = function() {
    $ionicLoading.show({
      template: 'Cargando informacion...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $scope.error = function() {
    $ionicLoading.show({
      template: 'Ingrese fechas',
      duration:1000
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

 
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");

      
    });
  };  


 $http.get(host+"/agente/").success(function(response) {


    $scope.agente = response[0];

    console.log('hshshshhhs',$scope.agente)

    $scope.muestraestructura = true

    if ($scope.agente.nivel__nombre=='PRIVATE CLIENT ADVISOR'){


      $scope.muestraestructura = false
    }
  

    });

////Reportes

$scope.sacareportepopup = function() {

      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: ' <span style="color:#222;">Desde</span><label class="item item-input"><input ng-model="inicio" required ="true" ng-change="guardainicio(inicio)" type="date"></input></label><span style="color:#222;">Hasta</span><label class="item item-input"><input ng-model="fin" required="true" type="date" ng-change="guardafin(fin)"></input></label>',
         title: 'Ingrese intervalo de fechas a buscar',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

            { text: 'Genera CSV',type: 'button-light',onTap: function(e) {$scope.generareporte()}}
                     
         ]
      });

      // myPopup.then(function(res) {

        

      //   $scope.hide()

      //   //$scope.myGoBack()

      //   //location.reload();
         
      // });    
   };


    $scope.sacareporteclientepopup = function() {

      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: ' <span style="color:#222;">Desde</span><label class="item item-input"><input ng-model="inicio" required ="true" ng-change="guardainicio(inicio)" type="date"></input></label><span style="color:#222;">Hasta</span><label class="item item-input"><input ng-model="fin" required="true" type="date" ng-change="guardafin(fin)"></input></label>',
         title: 'Ingrese intervalo de fechas a buscar',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

            { text: 'Genera CSV',type: 'button-light',onTap: function(e) {$scope.generareportecitas()}}
                     
         ]
      });
  
   };


   $scope.sacareporteclienterealpopup = function() {

      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: ' <span style="color:#222;">Desde</span><label class="item item-input"><input ng-model="inicio" required ="true" ng-change="guardainicio(inicio)" type="date"></input></label><span style="color:#222;">Hasta</span><label class="item item-input"><input ng-model="fin" required="true" type="date" ng-change="guardafin(fin)"></input></label><br><ion-checkbox ng-show="muestraestructura" ng-click="guardaestructura(estru)" ng-model="estru">Con Equipo</ion-checkbox>',
         title: 'Ingrese intervalo de fechas a buscar',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

            { text: 'Genera CSV',type: 'button-light',onTap: function(e) {$scope.generareporteclientes()}}
                     
         ]
      });
  
   };


    $scope.sacareportepropuestaspopup = function() {

      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: ' <span style="color:#222;">Desde</span><label class="item item-input"><input ng-model="inicio" required ="true" ng-change="guardainicio(inicio)" type="date"></input></label><span style="color:#222;">Hasta</span><label class="item item-input"><input ng-model="fin" required="true" type="date" ng-change="guardafin(fin)"></input></label><br><ion-checkbox ng-show="muestraestructura" ng-click="guardaestructura(estru)" ng-model="estru">Con Equipo</ion-checkbox>',
         title: 'Ingrese intervalo de fechas a buscar',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

            { text: 'Genera CSV',type: 'button-light',onTap: function(e) {$scope.generareportepropuestas()}}
                     
         ]
      });
  
   };

       $scope.sacareportepospopup = function() {

      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '<ion-checkbox  ng-click="guardaenero(data)" ng-model="data.enero">Enero - Julio</ion-checkbox> <ion-checkbox  ng-click="guardafebrero(data)" ng-model="data.febrero"> Febrero - Agosto</ion-checkbox> <ion-checkbox  ng-click="guardamarzo(data)" ng-model="data.marzo">Marzo- Setiembre</ion-checkbox> <ion-checkbox  ng-click="guardaabril(data)" ng-model="data.abril">Abril - Octubre</ion-checkbox> <ion-checkbox  ng-click="guardamayo(data)" ng-model="data.mayo">Mayo- Noviembre</ion-checkbox> <ion-checkbox  ng-click="guardajunio(data)" ng-model="data.junio">Junio - Diciembre</ion-checkbox>',
         title: 'Seleccione periodo POS',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light',onTap: function(e) {$scope.hide()} },

            { text: 'Genera CSV',type: 'button-light',onTap: function(e) {$scope.generareportepos()}}
                     
         ]
      });
  
   };



   $scope.guardaenero=function(data){

    console.log(data)

    $localStorage.enero=data

    data.febrero=false
    data.marzo=false
    data.abril=false
    data.mayo=false
    data.junio=false


   }

   $scope.guardafebrero=function(data){

    console.log(data)

    $localStorage.febrero=data

    data.enero=false
    data.marzo=false
    data.abril=false
    data.mayo=false
    data.junio=false



   }

    $scope.guardamarzo=function(data){

    console.log(data)

    $localStorage.marzo=data

    data.enero=false
    data.febrero=false
    data.abril=false
    data.mayo=false
    data.junio=false


   }

   $scope.guardaabril=function(data){

    console.log(data)

    $localStorage.abril=data

    data.enero=false
    data.febrero=false
    data.marzo=false
    data.mayo=false
    data.junio=false


   }

   $scope.guardamayo=function(data){

    console.log(data)

    $localStorage.mayo=data

    data.enero=false
    data.febrero=false
    data.marzo=false
    data.abril=false
    data.junio=false


   }

    $scope.guardajunio=function(data){

    console.log(data)

    data.enero=false
    data.febrero=false
    data.marzo=false
    data.abril=false
    data.mayo=false


   }


   $scope.guardainicio=function(data){

    console.log(data)

    $localStorage.inicio=data
   }

   $scope.guardafin=function(data){

    console.log(data)

    $localStorage.fin=data


   }


  $scope.generareporte=function(){


    console.log('sacando....',$localStorage.inicio)


    if (!$localStorage.inicio){

      $scope.error()
    }

    if (!$localStorage.fin){

      $scope.error()

    }

    if ($localStorage.inicio && $localStorage.fin ){


          $scope.show()
    data={}

    data.inicio =$localStorage.inicio
    data.fin =$localStorage.fin


    //sacareportepopup

    console.log($localStorage.fin,$localStorage.inicio)


  $http({
          url: host+"/sacareportegerente",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

            doOpen('http://xiencias.com/file.csv')
            $scope.hide()


       })



    }




  }

    $scope.generareportecitas=function(){


    console.log('sacando....',$localStorage.inicio)


    if (!$localStorage.inicio){

      $scope.error()
    }

    if (!$localStorage.fin){

      $scope.error()

    }

    if ($localStorage.inicio && $localStorage.fin ){


          $scope.show()
    data={}

    data.inicio =$localStorage.inicio
    data.fin =$localStorage.fin


    //sacareportepopup

    console.log('reportecliente..',data)


  $http({
          url: host+"/sacareportecliente/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

            doOpen('http://xiencias.com/filecliente.csv')
            $scope.hide()


       })



    }




  }


      $scope.generareporteclientes=function(){


    console.log('sacando....',$localStorage.inicio)


    // if (!$localStorage.inicio){

    //   $scope.error()
    // }

    // if (!$localStorage.fin){

    //   $scope.error()

    // }

    //if ($localStorage.inicio && $localStorage.fin ){


          $scope.show()
    data={}

    //data.inicio =$localStorage.inicio
    //data.fin =$localStorage.fin
    //data.estructura=$localStorage.estructura


    //console.log('=$localStorage.estructura',$localStorage.estructura)


    //sacareportepopup

    console.log('reportecliente..',data)


  $http({
          url: host+"/sacareporteclientereal/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

            doOpen('http://xiencias.com/filecliente.csv')
            $scope.hide()


       })



   // }




  }


     $scope.generareportepropuestas=function(){


    console.log('sacando....',$localStorage.inicio)


    if (!$localStorage.inicio){

      $scope.error()
    }

    if (!$localStorage.fin){

      $scope.error()

    }

    if ($localStorage.inicio && $localStorage.fin ){


          $scope.show()
    data={}

    data.inicio =$localStorage.inicio
    data.fin =$localStorage.fin
    data.estructura=$localStorage.estructura


    console.log('=$localStorage.estructura',$localStorage.estructura)


    //sacareportepopup

    console.log('reportecliente..',data)


  $http({
          url: host+"/sacareportepropuestas/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

            doOpen('http://xiencias.com/filecliente.csv')
            $scope.hide()


       })



    }




  }

     $scope.generareportepos=function(){


    console.log('sacando....',$localStorage.enejul)


 
    delete $localStorage.enejul

          $scope.show()


          data=$scope.data





    //sacareportepopup

    console.log('reportecliente..',data)

  $http({
          url: host+"/sacareportepos/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

            doOpen('http://xiencias.com/filecliente.csv')
            $scope.hide()

       })

  


  }



  function doOpen(url){
        document.location.target = "_blank";
        document.location.href=url;
    }






}])

.controller('citasequipoCtrl', ['$scope', '$stateParams','$http','$ionicPopup','$ionicModal','$ionicLoading','$location','$filter',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams,$http,$ionicPopup,$ionicModal,$ionicLoading,$location,$filter) {

/////

$scope.tooltip = false

$scope.titulo=''

$scope.mousenp=function(data){

  console.log('daddadada')

  $scope.tooltip = true

  $scope.titulo=data

}


$scope.mouseleave=function(){

  console.log('daddadada')

  $scope.tooltip = false

}


$http.get(host+"/listaramos/").success(function(response) {$scope.ramos = response;});


$scope.propuesta = {}
////Modales


     $scope.showPopup = function() {


      console.log('ingresando.....')


      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in ramos" ng-click="obtenercia(item);cierra()">{{item.nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup1 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in cias" ng-click="obtenerproducto(item);cierra()">{{item.compania__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };

   $scope.showPopup2 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in productos" ng-click="obtenerpropuesta(item);cierra()">{{item.producto__nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };


 $scope.muestra =true





     $scope.obtenercia = function(data){


      console.log('obtener cia..',data)

      $scope.ramo = data

      $scope.propuesta.ramo = data

      $scope.producto=null

       $scope.cia =null

       
        $http.get(host+"/listacia/"+data.id).success(function(response) {

            $scope.cias = response;});

    }

    $scope.obtenerpropuesta = function(data){

      $scope.propuesta.producto = data

      $scope.producto=data
    }


    $scope.obtenerproducto = function(data){


        console.log('producto..',data)

        $scope.propuesta.cia =data


        $scope.cia =data

    
          $http.get(host+"/listaproducto/"+$scope.ramo.id+'/'+$scope.cia.compania).success(function(response) {

             $scope.productos = response;


            console.log('obtenerproducto',response)


         });

    }

      $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Guardando cliente...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


     $scope.guardacita = function() {
    $ionicLoading.show({
      template: 'Guardando cita...',
      duration:7000,
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  



      $scope.show = function() {
 
   $ionicLoading.show({ template: 'Se guardo con exito el cliente', noBackdrop: true, duration: 2000 })
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };  

      $scope.error = function(data) {
 
   $ionicLoading.show({ template: 'Completar ' + data, noBackdrop: true, duration: 2000 })
  };









   $scope.limpiar=function(data){


    console.log(data.direccion)

    data.direccion=null
    data.telefono=null
    data.last_name=null
    data.first_name=null
    data.fecha_inicio=null



   }

     
    $scope.agregacliente=function(data){


      console.log('agregando cliente,,,,',data)

      $scope.guarda()

      e=0
      console.log('hshsh',data)

      if (data.fecha_inicio==null){

        $scope.error('fecha de cita')

        e=1

      }

      if (data.first_name==null || data.first_name==''){

        $scope.error('nombre')

        e=1
      }


      if (e==0){


          $http({
          url: host+"/creacliente/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

          
            $scope.hide()
            

            $scope.muestra =true

            $scope.nuevoclientemodal.hide()

            $location.url('/agregaprospecto/'+data)


          })



      }


    }








//////


    $ionicModal.fromTemplateUrl('templates/nuevocliente.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {


      $scope.nuevoclientemodal = modal;


   });




           $scope.okCitaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo la cita correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

        //$scope.myGoBack()

        //location.reload();
         
      });    
   };

$scope.muestra= true
 $http.get(host+"/agentesequipo/").success(function(response) {$scope.agentesequipo = response;


    });

 $http.get(host+"/clientes/").success(function(response) {$scope.todosclientes = response;});


$scope.agarracliente=function(data){

  console.log('hdhhd',data.id)

  

  $http.get(host+"/listapropuestas/"+data.id).success(function(response) {


    console.log('listapropuestas',response)

    $scope.listapropuestas =response

    $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})


  });




}

$scope.agregaclientemodal = function(){



  $scope.nuevoclientemodal.show();


}




$scope.agregacita = function(data){

   $scope.guardacita()
  console.log('hshshs',data)

  data.propuestajson = $scope.propuesta

  $http({
                  url: host+"/creacitaequipo/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {



                    console.log(data)

                    //$scope.okCitaPopup()

                  // $scope.okPropuestaPopup()

                  // $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  // $scope.listapropuestas = response;

                  // $scope.okCitaPopup()

                  $location.url("/side-menu21/page19")




                  });



                


}



}])

   
.controller('seguimientoCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$ionicHistory','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$ionicHistory,$filter) {



    $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };


    $scope.seguimiento=1
    $scope.cierre = 2
    $scope.poliza=2


    $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});
    $http.get(host+"/modalidad/").success(function(response) {$scope.modalidad = response;});

    $scope.okCitaPopup = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo la cita correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {

         
      });    
   };

    $scope.segui = {}


    $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) { 


      $scope.cliente = response[0];


    });


    if(!$stateParams.propuesta){


    $location.url("/page10")


    }


    $http.get(host+"/detallepropuesta/"+$stateParams.propuesta).success(function(response) { $scope.propuesta = response[0];});



    $scope.agregacita=function(data){

      console.log('Hola como estas.....',data)

      $scope.segui={}

      $scope.segui.form = data
      $scope.segui.cliente = $stateParams.cliente
      $scope.segui.propuesta = $scope.propuesta

      $scope.segui.seguimiento=1



      if (data.prima_target){

        $scope.segui.cierre=1
        $scope.segui.seguimiento=0
      
      }

      if (data.fecha_poliza){

        $scope.segui.poliza=1
        $scope.segui.seguimiento=0
      
      }


      // data.observacion=''
      // data.fecha_cita=''


      console.log('seguimiento..',$scope.segui)


      $http({
                  url: host+"/creacita/",
                  data: $scope.segui,
                  method: 'POST',

                  }).
                  success(function(data) {

                    //$scope.okCitaPopup()

                    $location.url("/side-menu21/page19")
    
               })



    }

}])
   
.controller('clientesCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('seguimiento2Ctrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('creaCitaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('alertaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])


.controller('historialCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$filter','$ionicModal','$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$filter,$ionicModal,$ionicLoading) {

    $scope.guardacita = function() {
    $ionicLoading.show({
      template: 'Guardando cita',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $scope.que=function(){

    console.log('delta...')
  }


   $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("Cerre.....");
    });
  };  



    $scope.updatecita = function(data){


      console.log('updatecita..')

                  data.muestralabel=true



      $scope.guardacita()

   $http({
          url: host+"/updatecita/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {





            $scope.hide()

            $scope.reloadpage()

            $scope.closeModal()


       })





    }





    $http.get(host+"/agente/").success(function(response) {


    $scope.agente = response[0];
    console.log('data',$scope.agente)

    });

  $scope.show = function() {
    $ionicLoading.show({
      template: 'Cargando informacion...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $scope.error = function() {
    $ionicLoading.show({
      template: 'Ingrese fechas',
      duration:1000
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

 
 

  function doOpen(url){
        document.location.target = "_blank";
        document.location.href=url;
    }


   $scope.show()


$scope.eliminarcita = function(data){

  console.log('eliminando..')

    $http.get(host+"/eliminarcita/"+data.id).success(function(response) {

                //$scope.reloadpage()

                 $http.get(host+"/citasagente/").success(function(response) { $scope.citas = response;


                  $scope.modal.hide();




                });
      
                

                  });

}


////Modal

  $ionicModal.fromTemplateUrl('templates/detalledecitamodal.html', {
      scope: $scope,
      animation: 'slide-in-up'
   }).then(function(modal) {


      $scope.modal = modal;
   });


   $scope.cita ={}
  
   $scope.openModal = function(data) {

    $scope.show()

    $scope.cita = data

    if (data.fecha_cita){

      var parts =data.fecha_cita.split('/');

      $scope.cita.fecha_cita = new Date(parts[2], parts[1]-1 , parts[0]); 


    }

      

    if (data.fecha_poliza){

      var parts1 =data.fecha_poliza.split('/');

      $scope.cita.fecha_poliza = new Date(parts1[2], parts1[1]-1 , parts1[0]); 


    }


      $scope.hide()

      $scope.modal.show();
   };
  
   $scope.closeModal = function() {


    $scope.show()

      console.log('Cernandndndnnd')
      $http.get(host+"/citasagente/").success(function(response) { $scope.citas = response;


      $scope.modal.hide();

      $scope.hide()


      });


      
   };
  
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
  
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
  
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });

////

$scope.reloadpage=function(){

 

  $http.get(host+"/citasagente/").success(function(response) { $scope.citas = response;


  $scope.hide()


});

}


$http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});


$http.get(host+"/citasagente/").success(function(response) { 


$scope.citas = response;

$scope.hide()








});

$scope.iracita=function(data){

  $location.url("page20/"+data)

}

}])
   
.controller('citaCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$filter','$ionicHistory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$filter,$ionicHistory) {


console.log('citaCtrl')

$http.get(host+"/citasagente/").success(function(response) { 

  $scope.citas = response;

  $scope.cita = $filter('filter')($scope.citas,{"id" : $stateParams.cita})[0]

  //$scope.cita.fecha_cita = new Date($scope.cita.fecha_cita)

  // console.log('tipo de dato',typeof($scope.cita.fecha_cita))


});

 $scope.show = function() {
    $ionicLoading.show({
      template: 'Cargando datos..',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };




$scope.updatecita = function(data){



   $http({
          url: host+"/updatecita/",
          data: data,
          method: 'POST',

          }).
          success(function(data) {

          


       })





}



$scope.eliminarcita = function(data){

$scope.show()


  console.log('eliminando..')

    $http.get(host+"/eliminarcita/"+data.id).success(function(response) {
      
                //$ionicHistory.goBack();

                  });

}



}])

  .controller('fichaDeClienteCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$ionicModal','$ionicHistory','$filter','$ionicLoading', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$ionicModal,$ionicHistory,$filter,$ionicLoading) {

    
  ///


  delete $localStorage.item

  $scope.eliminapopup = function(data) {
      $scope.data = data


    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<p style="color:#000;">Estas seguro de eliminar este cliente con todas sus citas y propuestas</p>',
    

         scope: $scope,

         title:'Eliminar Cliente:',
         buttons: [
         { text: 'Eliminar',
           type: 'button-light',
           onTap: function(e) {

            console.log($scope.data)


             $http({
                  url: host+"/eliminacliente",
                  data: $scope.data,
                  method: 'POST',

                  }).
                  success(function(data) {

                    $location.url('side-menu21/clientes')

                      
    
               })

           }}
           

          
        ]

      });

   
   };

   $scope.show = function() {
    $ionicLoading.show({
      template: 'Cargando datos..',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };
  $scope.hide = function(){
    $ionicLoading.hide().then(function(){
       console.log("The loading indicator is now hidden");
    });
  };

  $scope.exito = function() {
    $ionicLoading.show({
      template: 'Ficha de cliente actualizada',
      duration:3000
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };

    $scope.guarda = function() {
    $ionicLoading.show({
      template: 'Actualizando cliente...',
    }).then(function(){
       console.log("The loading indicator is now displayed");
    });
  };


  $ionicModal.fromTemplateUrl('templates/my-modal.html', {
      scope: $scope,
      cache:false,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modalpropuesta = modal;
   });


  $scope.agregaModal = function(data) {

    $scope.modalpropuesta.show();

  };

  $scope.closeModal = function() {
      $scope.modalpropuesta.hide();
   };
  




  //



  $scope.parientes={}


  $scope.items = [{
  id: 1,
  label: 'Soltero',

}, {
  id: 2,
  label: 'Casado',

},
 {
  id: 3,
  label: 'Divorciado',

}];






  $scope.cambia=function(cliente,item){


    console.log(cliente,item)


    if(item.label=='Casado'){


    $scope.cliente.conyugeflag = true

      $scope.muestraconyuge=true

      $scope.cliente.estado_civil=1

      if($scope.cliente.conyuge==undefined){

        $scope.traeconyuge(cliente)
      }


    }

     if(item.label=='Soltero'){

      $scope.cliente.estado_civil=2

    }

     if(item.label=='Divorciado'){

      $scope.cliente.estado_civil=3

    }


    $localStorage.item = $scope.cliente.estado_civil
 


  }



  $scope.traeconyuge = function(data){

       $scope.openModal(data,'conyuge')

  }

    $scope.traehijo = function(data){


      $scope.emailcli = data.email

      $scope.fnacimientocli = data.fecha_nacimiento


       $scope.openModal(data,'hijo')
         


  }

   $scope.myGoBack = function() {
    $ionicHistory.goBack();
  };

    $scope.okficha = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup = $ionicPopup.show({
         template: '',
         title: 'Se guardo datos correctamente',

         scope: $scope,
            
         buttons: [
            { text: 'Cerrar',type: 'button-light'}
         ]
      });

      myPopup.then(function(res) {
         $scope.closeModal()
      });    
   };

      $scope.closeModal = function() {
      $scope.modal.hide();
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });



  $ionicModal.fromTemplateUrl('templates/parientes.html', {
      scope: $scope,
      animation: 'slide-in-up',
   }).then(function(modal) {
      $scope.modal = modal;



   });

   $scope.openModal = function(data,relacion) {

    
    console.log('openmodal',data)

    $scope.id_cliente=data.id

    $scope.emailcli = data.email

    $scope.fnacimientocli = data.fecha_nacimiento


    $scope.pariente ={}

    if (relacion=='conyuge'){

      $scope.relacionfamiliar = 'conyuge'

      $scope.titulopariente = 'Agregar Conyuge'

    }
    else{

       $scope.relacionfamiliar = 'hijo'

      $scope.titulopariente = 'Agregar Hijo'


    }
    

    $scope.propuesta = {}

    $scope.propuesta.cliente = data



    $scope.modal.show();
   

   };
    
   $scope.closeModal = function() {
      $scope.modal.hide();
   };
    
   //Cleanup the modal when we're done with it!
   $scope.$on('$destroy', function() {
      $scope.modal.remove();
   });
    
   // Execute action on hide modal
   $scope.$on('modal.hidden', function() {
      // Execute action
   });
    
   // Execute action on remove modal
   $scope.$on('modal.removed', function() {
      // Execute action
   });





    $http.get(host+"/listapropuestas/"+$stateParams.cliente).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})


                  $scope.productosdecliente=$filter('filter')(response,{"cierre" : 1})


                  });



$scope.traecli = function(){



    console.log('emailcli....',$scope.emailcli)

    $scope.show()

    $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) { 

    
    $scope.cliente = response[0];

    

    if ($scope.emailcli){

      $scope.cliente.email= $scope.emailcli
    }
    else{


      console.log('Email....:(')
    }
    

    console.log('traecli...',$scope.cliente)

    if ($scope.fnacimientocli){

          $scope.cliente.fecha_nacimiento = $scope.fnacimientocli

    }

    else{


            if ($scope.cliente.fecha_nacimiento){

            var parts =$scope.cliente.fecha_nacimiento.split('/');

            var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 

            $scope.cliente.fecha_nacimiento = mydate

            }

    }
    

    console.log('$scope.cliente.fecha_nacimiento',$scope.cliente.fecha_nacimiento)


    if ($scope.cliente.fecha_nacimiento_conyuge){

        var parts =$scope.cliente.fecha_nacimiento_conyuge.split('/');

        var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 

        $scope.cliente.fecha_nacimiento_conyuge = mydate


    }

    if ($scope.cliente.parientes){


      console.log('hshsh',$scope.cliente.parientes)

      for( c in $scope.cliente.parientes){

        var parts =$scope.cliente.parientes[c].fecha_nacimiento.split('/');

        var mydate = new Date(parts[2], parts[1] - 1, parts[0]); 

        $scope.cliente.parientes[c].fecha_nacimiento = mydate
      }


    }



    //$scope.cliente.fecha_nacimiento=new Date($scope.cliente.fecha_nacimiento);



    if($scope.cliente.estado_civil==1){

      $scope.item = $scope.items[1];
    }

    if($scope.cliente.estado_civil==2){

      $scope.item = $scope.items[0];
    }

     if($scope.cliente.estado_civil==3){

      $scope.item = $scope.items[2];
    }




    $scope.hide()



  });
}


$http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});

  $scope.traecli()

    $scope.edad=[]


    for (i = 0; i < 100; i++) { $scope.edad.push(i) } 

  $scope.agregapariente=function(data){

    $scope.hijos = []

    $scope.pariente={}


    console.log('pariente..',data)



    if ($scope.relacionfamiliar=='conyuge'){

      data.relacion=1
    }

     if ($scope.relacionfamiliar=='hijo'){

      data.relacion=2
    }

    data.cliente = $scope.id_cliente


     $http({
                  url: host+"/pariente/",
                  data: data,
                  method: 'POST',

                  }).
                  success(function(data) {

                    
                       $scope.traecli()

                         // if ($scope.relacionfamiliar=='hijo'){

                         //      $scope.traecli()
                         //    }

                       $scope.closeModal()
    
               })


 

    
  }

  $scope.muestra= true

  $scope.actualizarcliente=function(data){



    data.estado_civil = $localStorage.item

    //console.log('actualizarcliente',data,$localStorage.item)


    $scope.guarda()

    $scope.muestra = false


          $http({
                  url: host+"/creacliente/",
                  data: data,
                  method: 'PUT',

                  }).
                  success(function(data) {

                      

                      //$scope.okficha()

                      $scope.traecli()

                      $scope.muestra = true

                      $scope.hide()

                      $scope.exito()

                      //$scope.myGoBack()
    
               })
  }


}])
   

   
  .controller('miGestionCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicPopup','$ionicModal','$ionicHistory','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicPopup,$ionicModal,$ionicHistory,$filter) {

  ///


  $scope.aniotrae=2019


   $scope.anio=[

  {'id':0,'nombre':2018},
  {'id':1,'nombre':2019},  
  {'id':2,'nombre':2020},
  {'id':3,'nombre':2021}
  ]



  $scope.cambianio =function(data){


    console.log(data)

    $scope.traegestion(data.nombre)

    $scope.calculaproduccionporramo(data.nombre)




  }

  console.log('Mis Gestion....')

   $http.get(host+"/agente/").success(function(response) {

    $scope.agente=response[0];

    $scope.muestra = false

    console.log('migestion',$scope.agente)

     $scope.traegestion(2019)

          if ($scope.agente.nivel__nombre!='PRIVATE CLIENT ADVISOR'){

              $scope.muestra = true

              $scope.traegestion(2019)



          }

   });



   $scope.tit = 'META PERSONAL'





  Highcharts.chart('barras', {
     chart: {
        type: 'bar'
    },
    title: {
        text: null
    },
    subtitle: {
        text: null
    },
    xAxis: {
        categories: ['PRODUCCION YTD', $scope.tit, 'META REQUERIDA'],
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        lineWidth: 0,
        minorGridLineWidth: 0,
        gridLineColor: 'transparent',
        lineColor: 'transparent',
        minorTickLength: 0,
        tickLength: 0,
        title: {
            text: null,
            align: 'high'
        },
        labels: {
            enabled: false
        }
    },
    // tooltip: {
    //     pointFormat: '{series.name}: <b>$ {point:.1f}</b>',
    // },
    
    plotOptions: {
        bar: {

            groupPadding:0,
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true,
        enabled:false
    },
    credits: {
        enabled: false
    },
    series: [{
       
        data: [{
            name: 'VIDA',
            y: 0,
            color:'#ff9800'
        }, {
            name: 'SALUD',
            y: 0,
            color:'#84c3f3'
        },{
            name: 'INVESTMENTS',
            y: 0,
            color:'#2f77ba'
        }]
    }]
});



  ///Graficas



Highcharts.chart('pie3d', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {

            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            x: 0,
            y: 0,
            backgroundColor:'#FFFFFF'
           
        },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'VIDA',
            y: 0,
            color:'#ff9800'
        }, {
            name: 'SALUD',
            y: 0,
            color:'#84c3f3'
        },{
            name: 'INVESTMENTS',
            y: 0,
            color:'#34527c'
        },
        {
            name: 'RAMOS GENERALES',
            y: 0,
            color:'#9bb54f'
        }]
    }]
});




///

  $scope.traegestion =function(anio){

  $http.get(host+"/gestion/"+anio).success(function(response) {

    $scope.gestion = response;

    $scope.produccionmensual =response.produccionmensual

    console.log('$scope.produccionmensual',$scope.produccionmensual)

    $scope.produccioncias = response.cias

    $scope.produccionramos = response.ramos

    console.log('kkk',$scope.produccionmensual,$scope.produccioncias,$scope.produccionramos)

    var chart1 = $('#barras').highcharts();

    chart1.series[0].data[0].update(response.ytdavance)
    chart1.series[0].data[1].update(response.meta_personal)
    chart1.series[0].data[2].update(response.meta_requerida)


  });



  $scope.calculaproduccionporramo(2019)


 


  }


     $scope.calculaproduccionporramo =function(anio){

     $http.get(host+"/calculaproduccionporramo/"+anio).success(function(response) {



      var chart2 = $('#pie3d').highcharts();



      if (response[0]['prima_target'][0]){chart2.series[0].data[0].update(response[0]['prima_target'][0]['total'])}
     else{chart2.series[0].data[0].update(0)}

      if (response[1]['prima_target'][0]){chart2.series[0].data[1].update(response[1]['prima_target'][0]['total'])}
     else{chart2.series[0].data[1].update(0)}

      if (response[2]['prima_target'][0]){chart2.series[0].data[2].update(response[2]['prima_target'][0]['total'])}
     else{chart2.series[0].data[2].update(0)}

    if (response[3]['prima_target'][0]){chart2.series[0].data[3].update(response[3]['prima_target'][0]['total'])}
     else{chart2.series[0].data[3].update(0)}
     
  })


   }



    $scope.traegestionequipo =function(anio){







     $http.get(host+"/gestionequipo/"+anio).success(function(response) {

    $scope.gestion = response;

    $scope.produccionmensual =response.produccionmensual

    console.log('$scope.produccionmensual',$scope.produccionmensual)

    $scope.produccioncias = response.cias

    $scope.produccionramos = response.ramos


    console.log('kkk',$scope.produccionmensual,$scope.produccioncias,$scope.produccionramos)


     var chart1 = $('#barras').highcharts();


     chart1.series[0].data[0].update(response.ytdavance)
     chart1.series[0].data[1].update(response.meta_personal)
     chart1.series[0].data[2].update(response.meta_requerida)

     $scope.tit ='META EQUIPO'

     console.log('jdjdjjd',$scope.tit)


  });



      $http.get(host+"/calculaproduccionporramoequipo/").success(function(response) {



      var chart2 = $('#pie3d').highcharts();



      if (response[0]['prima_target'][0]){chart2.series[0].data[0].update(response[0]['prima_target'][0]['total'])}
     else{chart2.series[0].data[0].update(0)}

      if (response[1]['prima_target'][0]){chart2.series[0].data[1].update(response[1]['prima_target'][0]['total'])}
     else{chart2.series[0].data[1].update(0)}

      if (response[2]['prima_target'][0]){chart2.series[0].data[2].update(response[2]['prima_target'][0]['total'])}
     else{chart2.series[0].data[2].update(0)}

      if (response[3]['prima_target'][0]){chart2.series[0].data[3].update(response[3]['prima_target'][0]['total'])}
     else{chart2.series[0].data[3].update(0)}



   
     
  })


      }

  





}])
   
.controller('resumenDeNegociosCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('detalleNegocioCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
    
    


}])
   


.controller('datosAdicionalesClienteCtrl', ['$scope','$stateParams','$http','$localStorage','$location', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location) {
    


}])
   
.controller('metricasCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$state','$filter','$ionicPopup', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller

function ($scope,$stateParams,$http,$localStorage,$location,$state,$filter,$ionicPopup) {



  $http.get(host+"/agente/").success(function(response) {

    $scope.agente=response[0];


    console.log('$scope.agente',$scope.agente)


   });



  $scope.mes =[

  {'id':1,'nombre':'Enero'},
  {'id':2,'nombre':'Febrero'},
  {'id':3,'nombre':'Marzo'},
  {'id':4,'nombre':'Abril'},
  {'id':5,'nombre':'Mayo'},
  {'id':6,'nombre':'Junio'},
  {'id':7,'nombre':'Julio'},
  {'id':8,'nombre':'Agosto'},
  {'id':9,'nombre':'Setiembre'},
  {'id':10,'nombre':'Octubre'},
  {'id':11,'nombre':'Noviembre'},
  {'id':12,'nombre':'Diciembre'}
  ]



    $scope.anioxxx=[

  {'id':0,'nombre':2018},
  {'id':1,'nombre':2019},  
  {'id':2,'nombre':2020},
  {'id':3,'nombre':2021}
  ]

  $scope.aniotrae = 2019 



  $scope.sacasemana =function(data){


    console.log(data)



    $http.get(host+"/calculo/"+data.numero).success(function(response) {$scope.calculo = response;

  

    });

  }

  $scope.sacaanio =function(data){


    console.log(data)





    data=data.nombre



    $http.get(host+"/calculoanio/"+data).success(function(response) {$scope.calculoanio = response;



      $scope.calculoanio.ncitasnuevas=$scope.calculoanio.ncitas2018nuevo
      $scope.calculoanio.ncitasseguimiento=$scope.calculoanio.ncitas2018seguimiento
      $scope.calculoanio.ncitaspos=$scope.calculoanio.ncitas2018pos
      $scope.calculoanio.ncitas2018equipo=$scope.calculoanio.ncitas2018equipo

 

    

    });

  }


  $scope.sacaanio({id: 1, nombre: 2019})


  $http.get(host+"/calculomes/").success(function(response) {$scope.calculomes = response;

  

  $scope.ncitasmes = JSON.parse(response.ncitasmes)

  $scope.nseguimientomes = JSON.parse(response.nseguimientomes)

  $scope.nposmes = JSON.parse(response.nposmes)

    });


$scope.calculacitasmes = function(mes){

    $http.get(host+"/calculacitas/"+mes).success(function(response) {

      return response;

    });

}



//$scope.sacames(9)

$scope.sacames =function(data){



     console.log('MES.....',data)

    $scope.messeleccionadoid=data.id
    
    $scope.messeleccionado=data.nombre


     $http.get(host+"/calculacitasanio/"+data.id+'/'+$scope.aniotrae).success(function(response) {


       $scope.ncmes = response['ncitasmes']

       $scope.nsmes = response['nseguimientomes']

       $scope.ncitasequipo = response['ncitasequipo']

       $scope.npmes = response['nposmes']

       console.log('entreeeee...')

       $scope.cierra()


    });





  }

  hoy=new Date()

  console.log('kskkksksksksksks',hoy.getMonth())

  if (hoy.getMonth()==0){ 

    nombre  = 'Enero' ; $scope.i=$scope.mes[1]
   

  }
   if (hoy.getMonth()==1){ nombre  = 'Febrero' ; $scope.i=$scope.mes[2]}
  if (hoy.getMonth()==2){ nombre  = 'Marzo' ; $scope.i=$scope.mes[3]}
  if (hoy.getMonth()==3){ nombre= 'Abril' ; $scope.i=$scope.mes[4]}
  if (hoy.getMonth()==4){ nombre ='Mayo' ; $scope.i=$scope.mes[5]}
  if (hoy.getMonth()==5){ nombre='Junio' ; $scope.i=$scope.mes[6]}
  if (hoy.getMonth()==6){ nombre = 'Julio' ; $scope.i=$scope.mes[7]}
  if (hoy.getMonth()==7){ nombre = 'Agosto' ; $scope.i=$scope.mes[8]}
  if (hoy.getMonth()==8){ nombre = 'Setiembre' ; $scope.i=$scope.mes[9]}
  if (hoy.getMonth()==9){ nombre = 'Octubre' ; $scope.i=$scope.mes[10]}
  if (hoy.getMonth()==10){ nombre = 'Noviembre' ; $scope.i=$scope.mes[11]}
  if (hoy.getMonth()==11){ nombre = 'Diciembre' ;$scope.i=$scope.mes[12]}

  $scope.numeroanio = hoy.getMonth()+1


    $scope.showPopup2 = function() {
      $scope.data = {}
    
      // Custom popup
      var myPopup1 = $ionicPopup.show({
         template: '<li style="text-decoration: none;list-style: none;padding: 11px;border: 1px solid green;border-radius: 2px;color: #000;margin-bottom: 2px;" ng-repeat="item in mes" ng-click="sacames(item);">{{item.nombre}}</li>',
         
    

         scope: $scope,
         title:'Seleccione:',
         buttons: [
         { text: 'Cerrar',
           type: 'button-balanced'
          },
        ]

      });


       $scope.cierra=function(){
          myPopup1.close()
         }   
   };


$scope.ix={id: $scope.numeroanio, nombre: 2019}


  $scope.cambianio =function(data){

    console.log('cambianio',data)

    $scope.aniotrae=data.nombre

    $scope.obtienedatos(data.nombre)

    $scope.obtienetermometro(data.nombre)

    $scope.sacames({id: $scope.messeleccionadoid, nombre: $scope.messeleccionado})

    $scope.numeroanio=0

    $scope.sacaanio({id: 1, nombre: data.nombre})

  }

   $scope.sacames({id: hoy.getMonth()+1, nombre: nombre})

  $scope.sacaanio({id: 1, nombre: 2019})


  $http.get(host+"/semanas/").success(function(response) {$scope.semanas = response;

  
  });


  


  $scope.obtienedatos =function(anio){

  $http.get(host+"/semanasresumen/"+anio).success(function(response) {$scope.semanasresumen = response;

  
  });

}

$scope.obtienedatos(2019)




  $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});


  $scope.obtienetermometro =function(data){

  $http.get(host+"/termometro/"+data).success(function(response) {$scope.termometro = response;



    console.log('termometro..',response)


    $scope.estado=response.estado

    if (response.estado=='alerta'){

      $scope.colorestado='red'

      $scope.estadotexto = '<span style="color:#fff !important;"> TU PORCENTAJE DE NUEVOS PROSPECTOS ES MENOR AL SUGERIDO </span>'
    }

    if (response.estado=='exito'){

      $scope.colorestado='green'

      $scope.estadotexto = '<span style="color:#fff !important;">EXCELENTE TRABAJO TU NUMERO DE PROSPECTOS ES SUPERIOR AL PROMEDIO!</span>'
    }

    if (response.estado=='umbral'){

      $scope.colorestado='blue'

      $scope.estadotexto ='<span style="color:#fff !important;">FELICITACIONES, HAZ ALCANZADO EL NUMERO DE PROSPECTOS SUGERIDOS</span>' 
    
    }


     $scope.medida=response.porcentaje

     $scope.porcentajeesperado=response.porcentajeesperado

     $scope.porcentajereal=response.porcentaje


  });

}


$scope.obtienetermometro(2019)

  ///Graficas

Highcharts.chart('containerx', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: null
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    legend: {

            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'top',
            x: 0,
            y: 0,
            backgroundColor:'#FFFFFF'
           
        },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'VIDA',
            y: 0,
            color:'#ff9800'
        }, {
            name: 'SALUD',
            y: 0,
            color:'#84c3f3'
        },{
            name: 'INVESTMENTS',
            y: 0,
            color:'#34527c'
        },{
            name: 'RAMOS GENERALES',
            y: 0,
            color:'#9bb54f'
        }]
    }]
});



$http.get(host+"/calculapropuestasporramo/").success(function(response) {

  console.log('data...',response)

     var chart1 = $('#containerx').highcharts();


     console.log('cambios...',chart1.series[0].data[0])

     chart1.series[0].data[0].update(response[0]['total'])
     chart1.series[0].data[1].update(response[1]['total'])
     chart1.series[0].data[2].update(response[2]['total'])
     chart1.series[0].data[3].update(response[3]['total'])
 

})


  


}])
   



.controller('propuestaSaludCtrl', ['$scope','$stateParams','$http','$localStorage','$location','$ionicHistory','$filter', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location,$ionicHistory,$filter) {



  $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});

  $scope.propuesta=$stateParams.propuesta


  $scope.iracliente =function(data){

    

    $location.url('fichacliente/'+data)
  }


  $scope.eliminapropuesta = function(data){


      console.log('eliminapropuesta.....')

      $http.get(host+"/eliminapropuesta/"+data).success(function(response) {

     


      });

      $location.path('intro')


  }


    $http.get(host+"/detallepropuesta/"+$scope.propuesta).success(function(response) {

    $scope.detallepropuesta = response[0];

    });

}])
   
.controller('misCierresCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('citaPOSCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('inicioCtrl', ['$scope','$stateParams','$http','$localStorage','$location', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope,$stateParams,$http,$localStorage,$location) {



    $http.get(host+"/iconos/").success(function(response) {$scope.iconos = response;});


    $http.get(host+"/agente/").success(function(response) {$scope.agente = response[0];});


}])
   


  .controller('listaDePropuestasCtrl', ['$scope','$stateParams','$http','$localStorage','$location', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
function ($scope,$stateParams,$http,$localStorage,$location) {




      $scope.cliid =$stateParams.cliente

      $http.get(host+"/cliente/"+$stateParams.cliente).success(function(response) {

      $scope.clientepropuesta = response[0];

      $scope.traepropuestas($stateParams.cliente)

      });





 




      $scope.traepropuestas = function(data){


      $http.get(host+"/listapropuestas/"+data).success(function(response) {

                  $scope.listapropuestas = response;

                  $scope.listapropuestas=$filter('filter')($scope.listapropuestas,{"cierre" : 0})



                  });

    }


}])
   
.controller('listaDePropuestasPOSCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('pagetestCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
 