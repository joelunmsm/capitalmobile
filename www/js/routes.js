angular.module('app.routes', ['ngStorage','highcharts-ng'])

.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }])

.config(function($stateProvider, $urlRouterProvider,$httpProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    

    .state('login', {
      url: '/page1',
      templateUrl: 'templates/login.html',
      controller:'loginCtrl'
    })
        .state('capital', {
      url: '/side-menu21',
      templateUrl: 'templates/capital.html',
      controller:'capitalCtrl'
    })

      .state('brokermenu', {
      url: '/brokermenu',
      templateUrl: 'templates/broker.html',
      controller:'brokerCtrl'
    })
        .state('agenda', {
      url: '/page4',
      templateUrl: 'templates/agenda.html',
      controller:'loginCtrl'
    })
        .state('prospectoDeCliente', {
      url: '/nuevapropuesta',
      templateUrl: 'templates/prospectoDeCliente.html',
      controller:'prospectoDeClienteCtrl'
    })
        .state('nuevaPropuesta', {
      url: '/page14',
      templateUrl: 'templates/nuevaPropuesta.html',
      controller:'nuevaPropuestaCtrl'
    })



        .state('nuevaPropuestaSEG', {
      url: '/page31',
      templateUrl: 'templates/nuevaPropuestaSEG.html',
      controller:'loginCtrl'
    })
        .state('pOS', {
      url: '/pos',
      templateUrl: 'templates/pos.html',
      controller:'prospectoDeClienteCtrl'
    })
        .state('tabs', {
      url: '/tabs',
      templateUrl: 'templates/tabs.html'
    })
         .state('nuevocliente', {
      url: '/nuevocliente',
      cache:false,
      templateUrl: 'templates/nuevocliente_1.html',
      controller:'nuevoclienteCtrl'
    })

        .state('clienteantiguo', {
      url: '/clienteantiguo',
      cache:false,
      templateUrl: 'templates/clienteantiguo.html',
      controller:'clienteantiguoCtrl'
    })



    
    .state('capital.miPerfil', {
      url: '/page11',
      views: {
        'side-menu21': {
          templateUrl: 'templates/miPerfil.html',
          controller:'capitalCtrl'
        }
      }
    })

    .state('capital.notificacion', {
      url: '/notificacion/:data',
      cache:false,
      views: {
        'side-menu21': {
          cache:false,
          templateUrl: 'templates/notificacion.html',
          controller:'notificacionCtrl'
        }
      }
    })

    .state('brokermenu.miPerfil', {
      url: '/perfilbroker',
      views: {
        'brokermenu': {
          templateUrl: 'templates/perfilbroker.html',
          controller:'perfilbrokerCtrl'
        }
      }
    })


    .state('brokermenu.biblioteca', {
      url: '/biblioteca',
      views: {
        'brokermenu': {
          templateUrl: 'templates/biblioteca.html',
          controller:'bibliotecaCtrl'
        }
      }
    })


  

     .state('capital.seguimiento', {
      url: '/seguimiento',
      views: {
        'side-menu21': {
          templateUrl: 'templates/seguimiento.html',
          controller:'prospectoDeClienteCtrl'
        }
      }
    })
        .state('intro', {
      url: '/page9',
      templateUrl: 'templates/intro.html',
      controller:'loginCtrl'
    })
        .state('seguimiento', {
      url: '/page10',
      templateUrl: 'templates/seguimiento.html',
      controller:'prospectoDeClienteCtrl'
    })
        .state('capital.clientes', {
      url: '/clientes',
      cache:false,
      views: {
        'side-menu21': {
          templateUrl: 'templates/clientes.html',
          controller:'prospectoDeClienteCtrl'
        }
      }
    })
        .state('seguimiento2', {
      url: '/seguimiento2',
      templateUrl: 'templates/seguimiento2.html',
      controller:'seguimientoCtrl',  
      cache:false,
      params: {
        cliente: null,
        propuesta:null
      }
    })
        .state('creaCita', {
      url: '/page12',
      templateUrl: 'templates/creaCita.html',
      controller:'loginCtrl'
    })
        .state('alerta', {
      url: '/page18',
      templateUrl: 'templates/alerta.html',
      controller:'loginCtrl'
    })
        .state('capital.historial', {
      url: '/page19',
      cache:false,
      views: {
        'side-menu21': {
          templateUrl: 'templates/historial.html',
          controller:'historialCtrl'
        }
      }
    })

           .state('capital.reporte', {
      url: '/reporte',
      views: {
        'side-menu21': {
          templateUrl: 'templates/reporte.html',
          controller:'reporteCtrl'
        }
      }
    })
      .state('capital.biblioteca', {
      url: '/biblioteca',
      views: {
        'side-menu21': {
          templateUrl: 'templates/biblioteca.html',
          controller:'bibliotecaCtrl'
        }
      }
    })
        .state('cita', {
      url: '/detallecita/:cita',
      templateUrl: 'templates/detallecita.html',
      controller:'citaCtrl',
      params: {
        cita: null
      }
    })
        .state('fichaDeCliente', {
      url: '/fichacliente/:cliente',
      templateUrl: 'templates/fichaDeCliente.html',
      controller:'fichaDeClienteCtrl',
       params: {
        cliente: null
      }
    })

         .state('detallecliente', {
      url: '/detallecliente/:cliente',
      templateUrl: 'templates/detallecliente.html',
      cache:false,
      controller:'fichaDeClienteCtrl',
       params: {
        cliente: null
      }
    })

        .state('agregaprospecto', {
      url: '/agregaprospecto/:cliente',
      templateUrl: 'templates/agregaprospecto.html',
      controller:'agregaprospectoCtrl',
      cache:false,
       params: {
        cliente: null
      }
    })


          .state('agregaprospectoantiguo', {
      url: '/agregaprospectoantiguo/:cliente',
      templateUrl: 'templates/agregaprospectoantiguo.html',
      controller:'agregaprospectoantiguoCtrl',
      cache:false,
       params: {
        cliente: null
      }
    })
        .state('capital.registroDeCitas', {
      url: '/menucitas',
      cache:false,
      views: {
        'side-menu21': {
          templateUrl: 'templates/registroDeCitas.html',
          controller:'registroDeCitasCtrl'
        }
      }
    })
        .state('capital.miGestion', {
      url: '/migestion',
      cache:false,
      views: {
        'side-menu21': {
          templateUrl: 'templates/miGestion.html',
          controller:'miGestionCtrl'
        }
      }
    })
        .state('capital.resumenDeNegocios', {
      url: '/page26',
      views: {
        'side-menu21': {
          templateUrl: 'templates/resumenDeNegocios.html',
          controller:'loginCtrl'
        }
      }
    })

 
        .state('datosAdicionalesCliente', {
      url: '/page17',
      templateUrl: 'templates/datosAdicionalesCliente.html',
      controller:'loginCtrl'
    })
        .state('capital.metricas', {
      url: '/metricas',
      cache:false,
      views: {
        'side-menu21': {
          templateUrl: 'templates/metricas.html',
          controller:'metricasCtrl'
        }
      }
    })
        .state('propuestaSalud', {
      url: '/detallepropuesta/:propuesta',
      templateUrl: 'templates/detallepropuesta.html',
      controller:'detallepropuestaCtrl',
      params: {
        propuesta: null
      }
    })

        .state('citasequipo', {
      url: '/citasequipo',
      cache:false,
      templateUrl: 'templates/citasequipo.html',
      controller:'citasequipoCtrl',
      params: {
        propuesta: null
      }
    })
        .state('capital.misCierres', {
      url: '/page28',
      views: {
        'side-menu21': {
          templateUrl: 'templates/misCierres.html',
          controller:'loginCtrl'
        }
      }
    })
        .state('citaPOS', {
      url: '/citapos/:cliente',
      templateUrl: 'templates/citaPOS.html',
      controller:'pOSCtrl',
      params: {
        cliente: null
      }
    })
        .state('inicio', {
      url: '/inicio',
      cache:false,
      templateUrl: 'templates/inicio.html',
      controller:'capitalCtrl'
    })

        .state('broker', {
      url: '/broker',
      cache:false,
      templateUrl: 'templates/broker.html',
      controller:'brokerCtrl'
    })

        .state('listaDePropuestas', {
      url: '/listapropuestas/:cliente',
      templateUrl: 'templates/listaDePropuestas.html',
      cache:false,
      controller:'prospectoDeClienteCtrl',
      params: {
        cliente: null
      }
    })




        .state('listaDePropuestasPOS', {
      url: '/page34',
      templateUrl: 'templates/listaDePropuestasPOS.html',
      controller:'loginCtrl'
    })
        .state('pagetest', {
      url: '/page33',
      templateUrl: 'templates/pagetest.html',
      controller:'loginCtrl'
    })
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/page1');

  host =  "http://xiencias.com:3000"

  //Django Auth JWT 

  $httpProvider.defaults.headers.post['Accept'] = 'application/json, text/javascript'; 

  $httpProvider.defaults.headers.post['Content-Type'] = 'multipart/form-data; charset=utf-8';



  
  $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage) {
        return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function(response) {
                    if(response.status === 401 || response.status === 403) {
                        $location.path('/signin');
                    }
                    return $q.reject(response);
                }
            };
        }]);



  


});